package pe.edu.uni.fiis.creaexamen.creaexamenws.dao;

import pe.edu.uni.fiis.creaexamen.creaexamenws.dto.Alumno;
import pe.edu.uni.fiis.creaexamen.creaexamenws.dto.Docente;
import pe.edu.uni.fiis.creaexamen.creaexamenws.dto.examen.*;

import java.util.List;

public interface CreaexamenDao {
    public Alumno registrarAlumno(Alumno alumno);
    public Docente registrarDocente(Docente docente);
    public Alumno loginAlumno(Alumno alumno);
    public Docente loginDocente(Docente docente);
    public List<Alumno> obtenerAlumnos();
    public List<Docente> obtenerDocentes();
    public EstructuraExamen guardarExamen(EstructuraExamen estructuraExamen);
    public Examen obtenerExamenporCod(Examen examen);
    public List<Examen> obtenerExamenesDocente(Docente docente);
    public Examen crearExamen(Examen examen);
    public ExamenAlumno guardarExamenAlumno(ExamenAlumno examenAlumno);
    public List<ExamenAlumno> obtenerExamenesAlumno(Alumno alumno);
    public Alumno obtenerAlumnoporUser(Alumno alumno);
    public Docente obtenerDocenteporUser(Docente docente);

    public Examen entrarExamen(Examen examen);
    public List<EstructuraExamen> obtenerPreguntas(Examen examen);
    public SolucionAlumno guardarSolucion(SolucionAlumno solucionAlumno);
    public AlumnoAsistencia crearAsistencia(AlumnoAsistencia alumnoAsistencia);
    public List<AlumnoAsistencia> obtenerAsistencia(Alumno alumno);
    public List<AlumnoAsistencia> obtenerAsistenciaporEx(Examen examen);
    public List<SolucionAlumno> obtenerSolucion(AlumnoAsistencia alumnoAsistencia);
    public AlumnoAsistencia insertarPuntaje(AlumnoAsistencia alumnoAsistencia);
}

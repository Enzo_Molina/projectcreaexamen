package pe.edu.uni.fiis.creaexamen.creaexamenws.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.edu.uni.fiis.creaexamen.creaexamenws.dao.CreaexamenDao;
import pe.edu.uni.fiis.creaexamen.creaexamenws.dao.CreaexamenDaoImpl;
import pe.edu.uni.fiis.creaexamen.creaexamenws.dto.Alumno;
import pe.edu.uni.fiis.creaexamen.creaexamenws.dto.Docente;
import pe.edu.uni.fiis.creaexamen.creaexamenws.dto.examen.*;

import java.util.List;


@Service
public class CreaexamenServiceImpl implements CreaexamenService {

    @Autowired
    private CreaexamenDao creadao;
    public Alumno registrarAlumno(Alumno alumno){
        return creadao.registrarAlumno(alumno);
    };

    public Docente registrarDocente(Docente docente){
        return creadao.registrarDocente(docente);
    }

    public Alumno loginAlumno(Alumno alumno){
        return creadao.loginAlumno(alumno);
    }

    public Docente loginDocente(Docente docente){
        return creadao.loginDocente(docente);
    };

    public List<Alumno> obtenerAlumnos(){
        return creadao.obtenerAlumnos();
    };

    public List<Docente> obtenerDocentes(){
        return creadao.obtenerDocentes();
    }

    public EstructuraExamen guardarExamen(EstructuraExamen estructuraExamen) {
        return creadao.guardarExamen(estructuraExamen);
    }
    public Examen obtenerExamenporCod(Examen examen){
        return creadao.obtenerExamenporCod(examen);
    };

    public List<Examen> obtenerExamenesDocente(Docente docente){
        return creadao.obtenerExamenesDocente(docente);
    };

    public Examen crearExamen(Examen examen){
        return creadao.crearExamen(examen);
    };

    public ExamenAlumno guardarExamenAlumno(ExamenAlumno examenAlumno){
      return creadao.guardarExamenAlumno(examenAlumno);
    };

    public List<ExamenAlumno> obtenerExamenesAlumno(Alumno alumno){
      return creadao.obtenerExamenesAlumno(alumno);
    };

    public Alumno obtenerAlumnoporUser(Alumno alumno){
        return creadao.obtenerAlumnoporUser(alumno);
    };

    public Docente obtenerDocenteporUser(Docente docente){
        return creadao.obtenerDocenteporUser(docente);
    };

    //a

    public Examen entrarExamen(Examen examen){
        return creadao.entrarExamen(examen);
    };


    public List<EstructuraExamen> obtenerPreguntas(Examen examen) {
        return creadao.obtenerPreguntas(examen);
    }

    public SolucionAlumno guardarSolucion(SolucionAlumno solucionAlumno) {
        return creadao.guardarSolucion(solucionAlumno);
    }

    public AlumnoAsistencia crearAsistencia(AlumnoAsistencia alumnoAsistencia) {
        return creadao.crearAsistencia(alumnoAsistencia);
    }
    public List<AlumnoAsistencia> obtenerAsistencia(Alumno alumno){
        return creadao.obtenerAsistencia(alumno);
    };

    public List<AlumnoAsistencia> obtenerAsistenciaporEx(Examen examen){
        return creadao.obtenerAsistenciaporEx(examen);
    };
    public List<SolucionAlumno> obtenerSolucion(AlumnoAsistencia alumnoAsistencia){
        return creadao.obtenerSolucion(alumnoAsistencia);
    };
    public AlumnoAsistencia insertarPuntaje(AlumnoAsistencia alumnoAsistencia){
        return creadao.insertarPuntaje(alumnoAsistencia);
    };
}

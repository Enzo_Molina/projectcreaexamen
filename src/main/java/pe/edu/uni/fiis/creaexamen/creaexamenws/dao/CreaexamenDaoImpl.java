package pe.edu.uni.fiis.creaexamen.creaexamenws.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import pe.edu.uni.fiis.creaexamen.creaexamenws.dto.Alumno;
import pe.edu.uni.fiis.creaexamen.creaexamenws.dto.Docente;
import pe.edu.uni.fiis.creaexamen.creaexamenws.dto.examen.*;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Repository
@Transactional
public class CreaexamenDaoImpl implements CreaexamenDao{
    @Autowired
    private JdbcTemplate jdbcTemplate;

    public @ResponseBody Alumno registrarAlumno(Alumno alumno){
        Alumno nuevoAlumno = null;
        String SQL = "insert into Alumno(username_Al,nombre,apellidopat,apellidomat,contraseña) "  +
                "VALUES ( '" + alumno.getUsername_Al() + "' , '" + alumno.getNombre() + "' , '" + alumno.getApellidopat() + "' , '" + alumno.getApellidomat() + "','" +alumno.getContrasenia() +"' )";
        try {
            Connection connection = jdbcTemplate.getDataSource().getConnection();
            Statement sentencia = connection.createStatement();
            ResultSet resultados = sentencia.executeQuery(SQL);
            while(resultados.next()){
                nuevoAlumno = new Alumno();
                nuevoAlumno.setUsername_Al(alumno.getUsername_Al());
                nuevoAlumno.setNombre(alumno.getNombre());
                nuevoAlumno.setApellidopat(alumno.getApellidopat());
                nuevoAlumno.setApellidomat(alumno.getApellidomat());
                nuevoAlumno.setContrasenia(alumno.getContrasenia());
            }
            sentencia.close();
            connection.close();

        }
        catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return nuevoAlumno;
    };

    public @ResponseBody Docente registrarDocente(Docente docente){
        Docente nuevoDocente = null;
        String SQL = "insert into Docente(username_Prof,nombre,apellidopat,apellidomat,contraseña) "  +
                "VALUES ( '" + docente.getUsername_Prof() + "' , '" + docente.getNombre() + "' , '" + docente.getApellidopat() + "' , '" + docente.getApellidomat() + "','"+ docente.getContrasenia() +"' )";
        try {
            Connection connection = jdbcTemplate.getDataSource().getConnection();
            Statement sentencia = connection.createStatement();
            ResultSet resultados = sentencia.executeQuery(SQL);
            while(resultados.next()){
                nuevoDocente = new Docente();
                nuevoDocente.setUsername_Prof(docente.getUsername_Prof());
                nuevoDocente.setNombre(docente.getNombre());
                nuevoDocente.setApellidopat(docente.getApellidopat());
                nuevoDocente.setApellidomat(docente.getApellidomat());
                nuevoDocente.setContrasenia(docente.getContrasenia());
            }
            sentencia.close();
            connection.close();

        }
        catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return nuevoDocente;
    };


    public @ResponseBody Alumno loginAlumno(Alumno alumno){
        Alumno verificarAlumno = null;
        String SQL = "select username_Al,nombre,apellidopat,apellidomat,contraseña from Alumno " +
                "where username_Al ='"+ alumno.getUsername_Al() + "'" + " AND contraseña ='" + alumno.getContrasenia() +"'";
        try {
            Connection connection = jdbcTemplate.getDataSource().getConnection();
            Statement sentencia = connection.createStatement();
            ResultSet resultados = sentencia.executeQuery(SQL);
            while(resultados.next()){
                verificarAlumno = new Alumno();
                verificarAlumno.setUsername_Al(resultados.getString("username_Al"));
                verificarAlumno.setNombre(resultados.getString("nombre"));
                verificarAlumno.setApellidopat(resultados.getString("apellidopat"));
                verificarAlumno.setApellidomat(resultados.getString("apellidomat"));
                verificarAlumno.setContrasenia(resultados.getString("contraseña"));

            }
            sentencia.close();
            connection.close();

        }
        catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return verificarAlumno;
    };

    public @ResponseBody Docente loginDocente(Docente docente){
        Docente verificarDocente = null;
        String SQL = "select username_Prof,nombre,apellidopat,apellidomat,contraseña from Docente " +
                "where username_Prof = '"+ docente.getUsername_Prof() + "'" + "AND contraseña = '" + docente.getContrasenia() +"'";
        try {
            Connection connection = jdbcTemplate.getDataSource().getConnection();
            Statement sentencia = connection.createStatement();
            ResultSet resultados = sentencia.executeQuery(SQL);
            while(resultados.next()){
                verificarDocente = new Docente();
                verificarDocente.setUsername_Prof(resultados.getString("username_Prof"));
                verificarDocente.setNombre(resultados.getString("nombre"));
                verificarDocente.setApellidopat(resultados.getString("apellidopat"));
                verificarDocente.setApellidomat(resultados.getString("apellidomat"));
                verificarDocente.setContrasenia(resultados.getString("contraseña"));

            }
            sentencia.close();
            connection.close();

        }
        catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return verificarDocente;
    };

    public List<Alumno> obtenerAlumnos(){
        String SQL = "select username_Al,nombre,apellidopat,apellidomat,contraseña from Alumno ";
        List<Alumno> listaAlum = new ArrayList<>();
        try {
            Connection connection = jdbcTemplate.getDataSource().getConnection();
            Statement sentencia = connection.createStatement();
            ResultSet resultados = sentencia.executeQuery(SQL);
            while(resultados.next()){
                Alumno alum = new Alumno();

                alum.setUsername_Al(resultados.getString("username_Al"));
                alum.setNombre(resultados.getString("nombre"));
                alum.setApellidopat(resultados.getString("apellidopat"));
                alum.setApellidomat(resultados.getString("apellidomat"));
                alum.setContrasenia(resultados.getString("contraseña"));

                listaAlum.add(alum);
            }
            sentencia.close();
            connection.close();
        }
        catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return listaAlum;
    }

    public List<Docente> obtenerDocentes(){
        String SQL = "select username_Prof,nombre,apellidopat,apellidomat ,contraseña from Docente ";
        List<Docente> listaDocen = new ArrayList<>();
        try {
            Connection connection = jdbcTemplate.getDataSource().getConnection();
            Statement sentencia = connection.createStatement();
            ResultSet resultados = sentencia.executeQuery(SQL);
            while(resultados.next()){
                Docente docen = new Docente();

                docen.setUsername_Prof(resultados.getString("username_Prof"));
                docen.setNombre(resultados.getString("nombre"));
                docen.setApellidopat(resultados.getString("apellidopat"));
                docen.setApellidomat(resultados.getString("apellidomat"));
                docen.setContrasenia(resultados.getString("contraseña"));
                listaDocen.add(docen);
            }
            sentencia.close();
            connection.close();
        }
        catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return listaDocen;
    }

    public EstructuraExamen guardarExamen(EstructuraExamen estructuraExamen) {

        String SQL="insert into estructuraExamen (cod_Examen,preguntatexto,p1,p2,p3,p4,p5)" +
                "values(?,?,?,?,?,?,?);";

        try {
            Connection con=jdbcTemplate.getDataSource().getConnection();
            PreparedStatement ps= con.prepareStatement(SQL);
            ps.setString(1,estructuraExamen.getCod_Examen());
            ps.setString(2,estructuraExamen.getTexto());
            ps.setString(3,estructuraExamen.getP1());
            ps.setString(4,estructuraExamen.getP2());
            ps.setString(5,estructuraExamen.getP3());
            ps.setString(6,estructuraExamen.getP4());
            ps.setString(7,estructuraExamen.getP5());
            ;
            ps.executeUpdate();
            ps.close();
            con.close();/*poner en todas para cerrar la conexion*/
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return estructuraExamen;
    }

    public @ResponseBody Examen obtenerExamenporCod(Examen examen){
        Examen nuevoExamen = null;
        String SQL = "select username_Prof,nombre,cod_Examen from examen " +
                "where cod_Examen ='"+ examen.getCod_Examen()+"'" ;
        try {
            Connection connection = jdbcTemplate.getDataSource().getConnection();
            Statement sentencia = connection.createStatement();
            ResultSet resultados = sentencia.executeQuery(SQL);
            while(resultados.next()){
                nuevoExamen = new Examen();
                nuevoExamen.setCod_Examen(resultados.getString("cod_Examen"));
                nuevoExamen.setNombre(resultados.getString("nombre"));
                nuevoExamen.setUsername_Prof(resultados.getString("username_Prof"));

            }
            sentencia.close();
            connection.close();

        }
        catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return nuevoExamen;
    }

    public List<Examen> obtenerExamenesDocente(Docente docente){
        String SQL = "select cod_Examen,username_Prof,nombre from examen " +
                "where username_Prof ='" + docente.getUsername_Prof() + "'" ;
        List<Examen> listaExamdoc = new ArrayList<>();
        try {
            Connection connection = jdbcTemplate.getDataSource().getConnection();
            Statement sentencia = connection.createStatement();
            ResultSet resultados = sentencia.executeQuery(SQL);
            while(resultados.next()){
                Examen examen1 = new Examen();
                examen1.setCod_Examen(resultados.getString("cod_Examen"));
                examen1.setUsername_Prof(resultados.getString("username_Prof"));
                examen1.setNombre(resultados.getString("nombre"));
                listaExamdoc.add(examen1);
            }
            sentencia.close();
            connection.close();
        }
        catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return listaExamdoc;
    };

    public @ResponseBody Examen crearExamen(Examen examen){
        Examen nuevoExamen2 = null;
            String SQL = "insert into examen (username_Prof,nombre,cod_Examen)" +
                    "values( '" + examen.getUsername_Prof() + "', '" + examen.getNombre() + "','" + examen.getCod_Examen() + "')";

            try {
                Connection connection = jdbcTemplate.getDataSource().getConnection();
                Statement sentencia = connection.createStatement();
            ResultSet resultados = sentencia.executeQuery(SQL);
            while(resultados.next()){
                nuevoExamen2 = new Examen();
                nuevoExamen2.setCod_Examen(resultados.getString("cod_Examen"));
                nuevoExamen2.setNombre(resultados.getString("nombre"));
                nuevoExamen2.setUsername_Prof(resultados.getString("username_Prof"));
            }
                sentencia.close();
                connection.close();
        }
        catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return nuevoExamen2;
    };


    public ExamenAlumno guardarExamenAlumno(ExamenAlumno examenAlumno){
        ExamenAlumno nuevoExamenAlumno = null;
        String SQL = "insert into examenAlumno (username_Al,cod_Examen,puntaje)" +
                "values('" + examenAlumno.getUsername_Al() + "','" + examenAlumno.getCod_Examen() + "','" + examenAlumno.getPuntaje() + "')";

        try {
            Connection connection = jdbcTemplate.getDataSource().getConnection();
            Statement sentencia = connection.createStatement();
            ResultSet resultados = sentencia.executeQuery(SQL);
            while(resultados.next()){
                nuevoExamenAlumno = new ExamenAlumno();
                nuevoExamenAlumno.setUsername_Al(resultados.getString("username_Al"));
                nuevoExamenAlumno.setCod_Examen(resultados.getString("cod_Examen"));
                nuevoExamenAlumno.setPuntaje(resultados.getFloat("puntaje"));
            }
            sentencia.close();
            connection.close();
        }
        catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return nuevoExamenAlumno;
    };

    public List<ExamenAlumno> obtenerExamenesAlumno(Alumno alumno){
        String SQL = "select cod_Examen,username_Al,puntaje from examenAlumno " +
                "where username_Al ='" + alumno.getUsername_Al() + "'" ;
        List<ExamenAlumno> listaExamAlum = new ArrayList<>();
        try {
            Connection connection = jdbcTemplate.getDataSource().getConnection();
            Statement sentencia = connection.createStatement();
            ResultSet resultados = sentencia.executeQuery(SQL);
            while(resultados.next()){
                ExamenAlumno examenAlum = new ExamenAlumno();
                examenAlum.setUsername_Al(resultados.getString("username_Al"));
                examenAlum.setCod_Examen(resultados.getString("cod_Examen"));
                examenAlum.setPuntaje(resultados.getFloat("puntaje"));
                listaExamAlum.add(examenAlum);
            }
            sentencia.close();
            connection.close();
        }
        catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return listaExamAlum;
    };

    public @ResponseBody Alumno obtenerAlumnoporUser(@RequestBody Alumno alumno){
        Alumno alumno1 = null;
        String SQL = "select username_Al,nombre,apellidopat,apellidomat,contraseña from Alumno " +
                "where username_Al='"+ alumno.getUsername_Al()+"'" ;
        try {
            Connection connection = jdbcTemplate.getDataSource().getConnection();
            Statement sentencia = connection.createStatement();
            ResultSet resultados = sentencia.executeQuery(SQL);
            while(resultados.next()){
                alumno1 = new Alumno();
                alumno1.setUsername_Al(resultados.getString("username_Al"));
                alumno1.setNombre(resultados.getString("nombre"));
                alumno1.setApellidopat(resultados.getString("apellidopat"));
                alumno1.setApellidomat(resultados.getString("apellidomat"));
                alumno1.setContrasenia(resultados.getString("contraseña"));
            }
            sentencia.close();
            connection.close();
        }
        catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return alumno1;
    };

    public Docente obtenerDocenteporUser(Docente docente){
        Docente docente1 = null;
        String SQL = "select username_Prof,nombre,apellidopat,apellidomat,contraseña from Docente " +
                "where username_Prof='"+ docente.getUsername_Prof()+"'" ;
        try {
            Connection connection = jdbcTemplate.getDataSource().getConnection();
            Statement sentencia = connection.createStatement();
            ResultSet resultados = sentencia.executeQuery(SQL);
            while(resultados.next()){
                docente1 = new Docente();
                docente1.setUsername_Prof(resultados.getString("username_Prof"));
                docente1.setNombre(resultados.getString("nombre"));
                docente1.setApellidopat(resultados.getString("apellidopat"));
                docente1.setApellidomat(resultados.getString("apellidomat"));
                docente1.setContrasenia(resultados.getString("contraseña"));
            }
            sentencia.close();
            connection.close();
        }
        catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return docente1;
    };

    //a
    public @ResponseBody Examen entrarExamen(Examen examen){
        Examen verificarCodigo= null;
        String SQL = "select username_Prof,nombre,cod_examen from examen " +
                "where cod_examen = '"+ examen.getCod_Examen() + "'";
        try {
            Connection connection = jdbcTemplate.getDataSource().getConnection();
            Statement sentencia = connection.createStatement();
            ResultSet resultados = sentencia.executeQuery(SQL);
            while(resultados.next()){
                verificarCodigo = new Examen();
                verificarCodigo.setUsername_Prof(resultados.getString("username_Prof"));
                verificarCodigo.setNombre(resultados.getString("nombre"));
                verificarCodigo.setCod_Examen(resultados.getString("cod_Examen"));
            }
            sentencia.close();
            connection.close();

        }
        catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return verificarCodigo;
    };



    public List<EstructuraExamen> obtenerPreguntas(Examen examen) {
        String SQL = "select cod_Examen, preguntatexto, p1, p2, p3, p4, p5 from estructuraExamen " +
                "where cod_Examen = '"+ examen.getCod_Examen() + "'";
        List<EstructuraExamen> listaestructura = new ArrayList<>();
        try {
            Connection connection = jdbcTemplate.getDataSource().getConnection();
            Statement sentencia = connection.createStatement();
            ResultSet resultados = sentencia.executeQuery(SQL);
            while(resultados.next()){
                EstructuraExamen preguntas = new EstructuraExamen();
                preguntas.setCod_Examen(resultados.getString("cod_Examen"));
                preguntas.setTexto(resultados.getString("preguntatexto"));
                preguntas.setP1(resultados.getString("p1"));
                preguntas.setP2(resultados.getString("p2"));
                preguntas.setP3(resultados.getString("p3"));
                preguntas.setP4(resultados.getString("p4"));
                preguntas.setP5(resultados.getString("p5"));
                listaestructura.add(preguntas);
            }
            sentencia.close();
            connection.close();
        }
        catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return listaestructura;
    }

    public @ResponseBody SolucionAlumno guardarSolucion(SolucionAlumno solucionAlumno) {
        SolucionAlumno solucionAlumno1 = null;
        String SQL = "insert into solucionAlumno(cod_Examen,username_Al,resp,n_preg)" +
                " values('"+ solucionAlumno.getCod_Examen() + "','" + solucionAlumno.getUsername_Al() + "','" + solucionAlumno.getResp() + "','" + solucionAlumno.getN_preg() + "'  )";
        try {
            Connection connection = jdbcTemplate.getDataSource().getConnection();
            Statement sentencia = connection.createStatement();
            ResultSet resultados = sentencia.executeQuery(SQL);
            while(resultados.next()){
                solucionAlumno1 = new SolucionAlumno();
                solucionAlumno1.setCod_Examen(solucionAlumno.getCod_Examen());
                solucionAlumno1.setUsername_Al(solucionAlumno.getUsername_Al());
                solucionAlumno1.setResp(solucionAlumno.getResp());
                solucionAlumno1.setN_preg(solucionAlumno.getN_preg());
            }
            sentencia.close();
            connection.close();
        }
        catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return solucionAlumno1;
    }

    public @ResponseBody AlumnoAsistencia crearAsistencia(AlumnoAsistencia alumnoAsistencia){
        AlumnoAsistencia nuevoAsistencia = null;
        String SQL = "insert into alumnoasistido (cod_Examen, username_Al)" +
                "values( '" + alumnoAsistencia.getCod_Examen() + "', '" + alumnoAsistencia.getUsername_Al() + "')";

        try {
            Connection connection = jdbcTemplate.getDataSource().getConnection();
            Statement sentencia = connection.createStatement();
            ResultSet resultados = sentencia.executeQuery(SQL);
            while(resultados.next()){
                nuevoAsistencia = new AlumnoAsistencia();
                nuevoAsistencia.setCod_Examen(resultados.getString("cod_Examen"));
                nuevoAsistencia.setUsername_Al(resultados.getString("username_Al"));
            }
            sentencia.close();
            connection.close();
        }
        catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return nuevoAsistencia;
    };

    public List<AlumnoAsistencia> obtenerAsistencia(Alumno alumno){

        String SQL = "select cod_Examen,username_Al,puntaje from alumnoasistido " +
                "where username_Al='"+ alumno.getUsername_Al()+"'" ;
        List<AlumnoAsistencia> listaAlumAsis = new ArrayList<>();
        try {
            Connection connection = jdbcTemplate.getDataSource().getConnection();
            Statement sentencia = connection.createStatement();
            ResultSet resultados = sentencia.executeQuery(SQL);
            while(resultados.next()){
                AlumnoAsistencia alumnoAsistencia = new AlumnoAsistencia();
                alumnoAsistencia.setUsername_Al(resultados.getString("username_Al"));
                alumnoAsistencia.setCod_Examen(resultados.getString("cod_Examen"));
                alumnoAsistencia.setPuntaje(resultados.getFloat("puntaje"));
                listaAlumAsis.add(alumnoAsistencia);
            }
            sentencia.close();
            connection.close();
        }
        catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return listaAlumAsis;
    };
    public List<AlumnoAsistencia> obtenerAsistenciaporEx(Examen examen){
        String SQL = "select cod_Examen,username_Al from alumnoasistido " +
                "where cod_Examen='"+ examen.getCod_Examen()+"'" ;
        List<AlumnoAsistencia> listaAlumAsisporEx = new ArrayList<>();
        try {
            Connection connection = jdbcTemplate.getDataSource().getConnection();
            Statement sentencia = connection.createStatement();
            ResultSet resultados = sentencia.executeQuery(SQL);
            while(resultados.next()){
                AlumnoAsistencia alumnoAsistencia2 = new AlumnoAsistencia();
                alumnoAsistencia2.setUsername_Al(resultados.getString("username_Al"));
                alumnoAsistencia2.setCod_Examen(resultados.getString("cod_Examen"));
                listaAlumAsisporEx.add(alumnoAsistencia2);
            }
            sentencia.close();
            connection.close();

        }
        catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return listaAlumAsisporEx;
    };

    public List<SolucionAlumno> obtenerSolucion(AlumnoAsistencia alumnoAsistencia){
        String SQL = "select cod_Examen,username_Al,resp,n_preg from solucionAlumno " +
                "where cod_Examen='"+ alumnoAsistencia.getCod_Examen()+"'" + "and username_Al = '" + alumnoAsistencia.getUsername_Al() + "'" ;
        List<SolucionAlumno> listaAlumsolucion = new ArrayList<>();
        try {
            Connection connection = jdbcTemplate.getDataSource().getConnection();
            Statement sentencia = connection.createStatement();
            ResultSet resultados = sentencia.executeQuery(SQL);
            while(resultados.next()){
                SolucionAlumno solucionAlumno = new SolucionAlumno();
                solucionAlumno.setUsername_Al(resultados.getString("username_Al"));
                solucionAlumno.setCod_Examen(resultados.getString("cod_Examen"));
                solucionAlumno.setResp(resultados.getString("resp"));
                solucionAlumno.setN_preg(resultados.getInt("n_preg"));
                listaAlumsolucion.add(solucionAlumno);
            }
            sentencia.close();
            connection.close();

        }
        catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return listaAlumsolucion;
    };

    public AlumnoAsistencia insertarPuntaje(AlumnoAsistencia alumnoAsistencia){

        String SQL = "update alumnoasistido set puntaje =" + alumnoAsistencia.getPuntaje() +
                " where username_Al ='" + alumnoAsistencia.getUsername_Al() + "' and cod_Examen ='"+ alumnoAsistencia.getCod_Examen() +"'";

        try {
            Connection connection = jdbcTemplate.getDataSource().getConnection();
            Statement sentencia = connection.createStatement();
            ResultSet resultados = sentencia.executeQuery(SQL);
            while(resultados.next()){
                alumnoAsistencia.setPuntaje(alumnoAsistencia.getPuntaje());
            }
            sentencia.close();
            connection.close();
        }
        catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return alumnoAsistencia;
    };
}

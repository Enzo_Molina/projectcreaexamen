package pe.edu.uni.fiis.creaexamen.creaexamenws.dto.examen;

import lombok.Data;

import java.util.List;

@Data
public class ListaSolucionAlumno {
    private List<SolucionAlumno> lista;
}

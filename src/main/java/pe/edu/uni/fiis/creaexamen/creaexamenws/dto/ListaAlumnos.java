package pe.edu.uni.fiis.creaexamen.creaexamenws.dto;

import lombok.Data;

import java.util.List;

@Data
public class ListaAlumnos {
    private List<Alumno> lista;
}

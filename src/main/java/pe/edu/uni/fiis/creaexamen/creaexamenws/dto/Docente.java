package pe.edu.uni.fiis.creaexamen.creaexamenws.dto;

import lombok.Data;

@Data
public class Docente {
    String username_Prof;
    String nombre;
    String apellidopat;
    String apellidomat;
    String contrasenia;
}

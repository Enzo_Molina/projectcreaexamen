package pe.edu.uni.fiis.creaexamen.creaexamenws.dto.examen;

import lombok.Data;

@Data
public class Examen {
    String username_Prof;
    String nombre;
    String cod_Examen;

}

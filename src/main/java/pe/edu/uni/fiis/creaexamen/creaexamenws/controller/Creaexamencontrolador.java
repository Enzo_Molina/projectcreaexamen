package pe.edu.uni.fiis.creaexamen.creaexamenws.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pe.edu.uni.fiis.creaexamen.creaexamenws.dto.Alumno;
import pe.edu.uni.fiis.creaexamen.creaexamenws.dto.Docente;
import pe.edu.uni.fiis.creaexamen.creaexamenws.dto.examen.*;
import pe.edu.uni.fiis.creaexamen.creaexamenws.service.CreaexamenService;
import pe.edu.uni.fiis.creaexamen.creaexamenws.service.CreaexamenServiceImpl;


import java.util.List;

@RestController
public class Creaexamencontrolador {
    @Autowired
    private CreaexamenService creaexamenService;

    @RequestMapping(value= "/registroAlumno",
            method = RequestMethod.POST,
            produces = "application/json;charset=utf-8")
    public @ResponseBody Alumno registrarAlumno(@RequestBody Alumno alumno){
        return creaexamenService.registrarAlumno(alumno);
    }


    @RequestMapping (value= "/registroDocente",
            method = RequestMethod.POST,
            produces = "application/json;charset=utf-8")
    public @ResponseBody Docente registrarDocente(@RequestBody Docente docente){
        return creaexamenService.registrarDocente(docente);
    }


    @RequestMapping (value= "/loginAlumno",
            method = RequestMethod.POST,
            produces = "application/json;charset=utf-8")
    public @ResponseBody Alumno loginAlumno(@RequestBody Alumno alumno){
        return creaexamenService.loginAlumno(alumno);
    }

    @RequestMapping (value= "/loginDocente",
            method = RequestMethod.POST,
            produces = "application/json;charset=utf-8")
    public @ResponseBody Docente loginDocente(@RequestBody Docente docente){
        return creaexamenService.loginDocente(docente);
    }

    @RequestMapping (value = "/obtenerAlumnos",
            method = RequestMethod.POST,
            produces = "application/json;charset=utf-8")
    public List<Alumno> obtenerAlumnos(){
        return creaexamenService.obtenerAlumnos();
    }

    @RequestMapping (value = "/obtenerDocentes",
            method = RequestMethod.POST,
            produces = "application/json;charset=utf-8")
    public List<Docente> obtenerDocentes(){
        return creaexamenService.obtenerDocentes();
    }

    @RequestMapping(value="/ingresarPregunta",
            method = RequestMethod.POST,
            produces = "application/json;charset=utf-8")
    public @ResponseBody EstructuraExamen guardarExamen(@RequestBody EstructuraExamen estructuraExamen){
        return creaexamenService.guardarExamen(estructuraExamen);
    }

    @RequestMapping(value="/obtenerExamenPorCod",
            method = RequestMethod.POST,
            produces = "application/json;charset=utf-8")
    public @ResponseBody Examen obtenerExamenporCod(@RequestBody Examen examen){
        return creaexamenService.obtenerExamenporCod    (examen);
    }

    @RequestMapping (value = "/obtenerExamenporDocente",
            method = RequestMethod.POST,
            produces = "application/json;charset=utf-8")
    public List<Examen> obtenerExamenesporDocente(@RequestBody Docente docente){
        return creaexamenService.obtenerExamenesDocente(docente);
    }

    @RequestMapping(value="/crearExamen",
            method = RequestMethod.POST,
            produces = "application/json;charset=utf-8")
    public @ResponseBody Examen crearExamen(@RequestBody Examen examen){
        return creaexamenService.crearExamen(examen);
    }

    @RequestMapping(value="/guardarExamenAlumno",
            method = RequestMethod.POST,
            produces = "application/json;charset=utf-8")
    public @ResponseBody ExamenAlumno guardarExamenAlumno(@RequestBody ExamenAlumno examenAlumno){
        return creaexamenService.guardarExamenAlumno(examenAlumno);
    }

    @RequestMapping(value="/obtenerExamenesAlumno",
            method = RequestMethod.POST,
            produces = "application/json;charset=utf-8")
    public @ResponseBody List<ExamenAlumno> obtenerExamenesAlumno(@RequestBody Alumno alumno){
        return creaexamenService.obtenerExamenesAlumno(alumno);
    }

    @RequestMapping(value="/obtenerAlumnosporUser",
            method = RequestMethod.POST,
            produces = "application/json;charset=utf-8")
    public @ResponseBody Alumno obtenerAlumnoporUser(@RequestBody Alumno alumno){
        return creaexamenService.obtenerAlumnoporUser(alumno);
    }

    @RequestMapping(value="/obtenerDocenteporUser",
            method = RequestMethod.POST,
            produces = "application/json;charset=utf-8")
    public @ResponseBody Docente obtenerDocenteporUser(@RequestBody Docente docente){
        return creaexamenService.obtenerDocenteporUser(docente);
    }

    @RequestMapping(value = "/entrarExamen",
            method = RequestMethod.POST,
            produces = "application/json;charset=utf-8")
    public @ResponseBody
    Examen entrarExamen(@RequestBody Examen examen) {
        return creaexamenService.entrarExamen(examen);
    }


    @RequestMapping(value = "/responderExamen",
            method = RequestMethod.POST,
            produces = "application/json;charset=utf-8")
    public @ResponseBody List<EstructuraExamen> obtenerPreguntas(@RequestBody Examen examen) {
        return creaexamenService.obtenerPreguntas(examen);
    }

    @RequestMapping(value = "/ingresarRespuesta",
            method = RequestMethod.POST,
            produces = "application/json;charset=utf-8")
    public @ResponseBody SolucionAlumno guardarSolucion(@RequestBody SolucionAlumno solucionAlumno) {
        return creaexamenService.guardarSolucion(solucionAlumno);
    }

    @RequestMapping(value = "/crearAsistencia",
            method = RequestMethod.POST,
            produces = "application/json;charset=utf-8")
    public @ResponseBody AlumnoAsistencia crearAsistencia(@RequestBody AlumnoAsistencia alumnoAsistencia) {
        return creaexamenService.crearAsistencia(alumnoAsistencia);
    }

    @RequestMapping(value = "/obtenerAsistencia",
            method = RequestMethod.POST,
            produces = "application/json;charset=utf-8")
    public @ResponseBody List<AlumnoAsistencia> obtenerAsistencia(@RequestBody Alumno alumno) {
        return creaexamenService.obtenerAsistencia(alumno);
    }

    @RequestMapping(value = "/obtenerAsistenciaporEx",
            method = RequestMethod.POST,
            produces = "application/json;charset=utf-8")
    public @ResponseBody List<AlumnoAsistencia> obtenerAsistenciaporEx(@RequestBody Examen examen) {
        return creaexamenService.obtenerAsistenciaporEx(examen);
    }

    @RequestMapping(value = "/obtenerSolucionAlumno",
            method = RequestMethod.POST,
            produces = "application/json;charset=utf-8")
    public @ResponseBody List<SolucionAlumno> obtenerSolucion(@RequestBody AlumnoAsistencia alumnoAsistencia) {
        return creaexamenService.obtenerSolucion(alumnoAsistencia);
    }

    @RequestMapping(value = "/insertarPuntaje",
            method = RequestMethod.POST,
            produces = "application/json;charset=utf-8")
    public @ResponseBody AlumnoAsistencia insertarPuntaje(@RequestBody AlumnoAsistencia alumnoAsistencia) {
        return creaexamenService.insertarPuntaje(alumnoAsistencia);
    }
}

package pe.edu.uni.fiis.creaexamen.creaexamenws.dto.examen;

import lombok.Data;

@Data
public class SolucionAlumno {
    private String cod_Examen;
    private String username_Al;
    private String resp;
    private Integer N_preg;
}

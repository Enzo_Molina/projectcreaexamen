package pe.edu.uni.fiis.creaexamen.creaexamenws.dto.examen;

import lombok.Data;

@Data
public class AlumnoAsistencia {
    private String cod_Examen;
    private String username_Al;
    private Float puntaje;
}

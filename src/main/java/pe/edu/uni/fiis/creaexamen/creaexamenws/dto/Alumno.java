package pe.edu.uni.fiis.creaexamen.creaexamenws.dto;

import lombok.Data;

@Data
public class Alumno {
    String username_Al;
    String nombre;
    String apellidopat;
    String apellidomat;
    String contrasenia;
}

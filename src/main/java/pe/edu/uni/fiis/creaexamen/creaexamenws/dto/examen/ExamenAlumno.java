package pe.edu.uni.fiis.creaexamen.creaexamenws.dto.examen;

import lombok.Data;

@Data
public class ExamenAlumno {
    String cod_Examen;
    String username_Al;
    float puntaje;
}

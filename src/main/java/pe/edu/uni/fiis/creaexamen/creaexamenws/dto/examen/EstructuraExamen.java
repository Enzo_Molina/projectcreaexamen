package pe.edu.uni.fiis.creaexamen.creaexamenws.dto.examen;

import lombok.Data;

@Data
public class EstructuraExamen {
    private String cod_Examen;
    private String texto;
    private String p1;
    private String p2;
    private String p3;
    private String p4;
    private String p5;

}
